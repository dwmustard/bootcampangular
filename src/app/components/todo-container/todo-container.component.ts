import { MainServiceService } from './../../services/main-service.service';
import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/interfaces/todo';

@Component({
  selector: 'app-todo-container',
  templateUrl: './todo-container.component.html',
  styleUrls: ['./todo-container.component.css']
})
export class TodoContainerComponent implements OnInit {
  todoList: Todo[] = [];

  constructor(private mainService: MainServiceService) { }

  ngOnInit(): void {
    this.getTodoList();
  }

  getTodoList() {
    this.todoList = this.mainService.getTodos();
  }
}
