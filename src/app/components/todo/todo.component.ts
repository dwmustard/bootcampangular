import { Todo } from './../../interfaces/todo';
import { MainServiceService } from './../../services/main-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  todoList: Todo[] = [];

  constructor(private mainService: MainServiceService) {
    this.getTodoList();
   }

  ngOnInit(): void{
  };

  getTodoList() {
    this.todoList = this.mainService.getTodos();
  }
}
